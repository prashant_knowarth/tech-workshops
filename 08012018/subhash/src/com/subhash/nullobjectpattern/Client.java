package com.subhash.nullobjectpattern;

import java.util.List;

public class Client {

	public static void main(String[] args) {
		
		List<Product> products = Database.getProducts();
		for(Product p : products) {
			System.out.println(p.sellAtMargin());
		}

	}

}
