package com.subhash.nullobjectpattern;

public class ProductFactory {
	
	public static Product getProduct(boolean isConcrete, String name, float price, int margin) {
		Product product = NoProduct.getProduct();
		if(isConcrete) {
			product = new Product(name, price, margin);
		}
		return product;
	}

}
