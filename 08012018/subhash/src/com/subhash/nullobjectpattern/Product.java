package com.subhash.nullobjectpattern;

public class Product {

	protected String name;
	private float price;
	private int margin;
	
	public Product() {
	}
	
	public Product(String name, float price, int margin) {
		this.name = name;
		this.price = price;
		this.margin = margin;
	}

	public boolean sellAtMargin() {
		System.out.println("Product [" + name + "] is sold at price[" + (price * margin) + "]");
		return true;
	}

	@Override
	public String toString() {
		return "Product[" + name + "]";
	}

}
