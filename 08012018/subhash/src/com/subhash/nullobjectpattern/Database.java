package com.subhash.nullobjectpattern;

import java.util.ArrayList;
import java.util.List;

public class Database {

	public static List<Product> getProducts() {
		List<Product> products = new ArrayList<Product>();
		products.add(ProductFactory.getProduct(false, "", 0.0f, 0));
		products.add(ProductFactory.getProduct(true, "Product 1", 10.2f, 5));
		products.add(ProductFactory.getProduct(true, "Product 2", 10.2f, 5));
		products.add(ProductFactory.getProduct(true, "Product 3", 10.2f, 5));
		return products;
	}

}
