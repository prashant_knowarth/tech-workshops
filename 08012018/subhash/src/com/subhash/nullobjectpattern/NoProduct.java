package com.subhash.nullobjectpattern;

public class NoProduct extends Product {

	private static NoProduct NO_PRODUCT = new NoProduct();
	
	private NoProduct() {
		this.name = "No product";
	}

	public static NoProduct getProduct() {
		return NO_PRODUCT;
	}

	@Override
	public boolean sellAtMargin() {
		System.out.println("No product");
		return false;
	}
}
