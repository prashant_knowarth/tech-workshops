package com.techworkshop;


public class RealProduct extends AbstractProduct {

	public RealProduct(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public boolean isNil() {
		return false;
	}
}
