package com.techworkshop;



public class ProductFactory {
	static AbstractProduct getProduct(String name) {
		final String[] products = {"Mouse", "Computer", "Printer"};
		for (int i = 0; i < products.length; i++) {
	         if (products[i].equalsIgnoreCase(name)){
				return new RealProduct(products[i]);
	         }
	    }
		return new NullProduct();
	}
}
