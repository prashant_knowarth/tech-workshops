package com.techworkshop;


public abstract class AbstractProduct {
	protected String name;
	public abstract boolean isNil();
	public abstract String getName();
}
