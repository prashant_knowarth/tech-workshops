package com.techworkshop;


class NullProduct extends AbstractProduct {

	@Override
	public String getName() {
		return "Not Available in Product Database";
	}

	@Override
	public boolean isNil() {
		return true;
	}
}
