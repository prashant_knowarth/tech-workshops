package com.techworkshop;


public class ProductDemo {
	public static void main(String[] args) {
		AbstractProduct product1 = ProductFactory.getProduct("Mouse");
		AbstractProduct product2 = ProductFactory.getProduct("Monitor");
		AbstractProduct product3 = ProductFactory.getProduct("Bottle");
		
		System.out.println(product1.getName());
		System.out.println(product2.getName());
		System.out.println(product3.getName());
	}
}
